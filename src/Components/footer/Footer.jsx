import React from "react";
import footer from "./footer.module.css";

class Footer extends React.Component {
  render() {
    return (
      <footer className={footer.container}>
        <div className={footer.body}>
          <div className={footer.email}>
            <a href="#" className={footer.emailLink}>
              feedback@dodopizza.com
            </a>
          </div>
          <div className={footer.box}>
            <ul className={footer.list}>
              <li className={footer.listElemTitle}>Додо Пицца</li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  О нас
                </a>
              </li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Додо-книга
                </a>
              </li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Блог «Сила ума»
                </a>
              </li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Додо ИС
                </a>
              </li>
            </ul>
          </div>
          <div className={footer.box}>
            <ul className={footer.list}>
              <li className={footer.listElemTitle}>Работа</li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  В пиццерии
                </a>
              </li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  В контакт - центре
                </a>
              </li>
            </ul>
          </div>
          <div className={footer.box}>
            <ul className={footer.list}>
              <li className={footer.listElemTitle}>Партнерам</li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Франшиза
                </a>
              </li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Инвестиции
                </a>
              </li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Поставщикам
                </a>
              </li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Предложить помещение
                </a>
              </li>
            </ul>
          </div>
          <div className={footer.box}>
            <ul className={footer.list} value="list1">
              <li className={footer.listElemTitle}>Это интересно</li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Экскурсии и мастер-классы
                </a>
              </li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Корпоративные заказы
                </a>
              </li>
            </ul>
          </div>
          <div className={footer.line}></div>
          <div className={footer.tabsContainer}>
            <img className={footer.tabsImg} src="img/facebook.png" alt="" />
            <a href="#"></a>
            <img className={footer.tabsImg} src="img/insta.png" alt="" />
            <a href="#"></a>
            <img className={footer.tabsImg} src="img/odnk.png" alt="" />
            <a href="#"></a>
            <img className={footer.tabsImg} src="img/vk.png" alt="" />
            <a href="#"></a>
            <img className={footer.tabsImg} src="img/yt.png" alt="" />
            <a href="#"></a>
          </div>
          <div className={footer.appsContainer}>
            <img className={footer.appsImg} src="img/googplay.png" alt="" />
            <a href="#"></a>
            <img className={footer.appsImg} src="img/appstore.png" alt="" />
            <a href="#"></a>
          </div>
          <div className={footer.line}></div>
          <div className={footer.box}>
            <ul className={footer.list} value="list2">
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Правовая информация
                </a>
              </li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Калорийность и состав
                </a>
              </li>
              <li className={footer.listElem}>
                <a href="" className={footer.listElemLink}>
                  Помощь
                </a>
              </li>
            </ul>
          </div>
          <div className={footer.logoContainer}>
            <img className={footer.logoImg} src="img/logo_footer.png" alt="" />
            <div className={footer.logoCopyright}>© 2021</div>
          </div>
        </div>
      </footer>
    );
  }
}
export default Footer;
