import React from "react";
import burgerMenu from "./menu.module.css";

class Menu extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className={burgerMenu.container}>
        <a className={burgerMenu.logoInMenu}></a>
        <a className={burgerMenu.closeElem}>+</a>
        <div className={burgerMenu.geoBlock}>
          <img
            src="../../../img/header_img/geo_icon.png"
            alt=""
            width="16"
            height="16"
            className={burgerMenu.geoImg}
          />
          <p className={burgerMenu.geoCity}>Москва</p>
        </div>
        <p className={burgerMenu.changeCity}>
          <a className={burgerMenu.changeLink} href="">
            Изменить
          </a>
        </p>
        <div className={burgerMenu.line}></div>
        <div className={burgerMenu.ratingBlock}>
          <p className={burgerMenu.ratingTime}> 33 мин</p>
          <img
            src="../../../img/header_img/rating_mob.png"
            width="134"
            height="27"
            alt=""
            className={burgerMenu.ratingImg}
          />
          <img
            src="../../../img/header_img/info.png"
            width="24"
            height="24"
            alt=""
            className={burgerMenu.ratingImg}
          />
        </div>
        <ul className={burgerMenu.menuList}>
          <li>
            <a className={burgerMenu.listElem} href="#">
              Профиль
            </a>
          </li>
          <li>
            <a className={burgerMenu.listElem} href="#">
              Мои бонусы
            </a>
          </li>
          <li>
            <a className={burgerMenu.listElem} href="#">
              Акции
            </a>
          </li>
          <li>
            <a className={burgerMenu.listElem} href="#">
              Контакты
            </a>
          </li>
          <li>
            <a className={burgerMenu.listElem} href="#">
              Работа в Додо
            </a>
          </li>
          <li>
            <a className={burgerMenu.listElem} href="#">
              Франшиза
            </a>
          </li>
          <li>
            <a className={burgerMenu.listElem} href="#">
              О нас
            </a>
          </li>
        </ul>
      </div>
    );
  }
}
export default Menu;
