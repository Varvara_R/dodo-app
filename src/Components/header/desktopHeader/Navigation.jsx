import deskHeader from "./desktopHeader.module.css";
function Navigation() {
  return (
    <>
      <nav className={deskHeader.navigation}>
        <ul className={deskHeader.navList}>
          <li>
            <a href="#do__pizza" className={deskHeader.navElem}>
              Пицца
            </a>
          </li>
          <li>
            <a href="#do__combo" className={deskHeader.navElem}>
              Комбо
            </a>
          </li>
          <li>
            <a href="#do__appetizers" className={deskHeader.navElem}>
              Закуски
            </a>
          </li>
          <li>
            <a href="#do__desserts" className={deskHeader.navElem}>
              Десерты
            </a>
          </li>
          <li>
            <a href="#do__beverages" className={deskHeader.navElem}>
              Напитки
            </a>
          </li>
          <li>
            <a href="" className={deskHeader.navElem}>
              Другие товары
            </a>
          </li>
        </ul>
        <button className={deskHeader.shopCartBtn}>
          Корзина
          <img
            src="img/empty_shop_cart.png"
            height="203"
            width="298"
            className={deskHeader.emptyCart}
          />
          <div className={deskHeader.separationShopCart}>
            <div className={deskHeader.separatorLine}></div>
            <p className={deskHeader.shopCartCounter}>1</p>
          </div>
        </button>
      </nav>
    </>
  );
}
export default Navigation;
