import deskHeader from "./desktopHeader.module.css";
import headerLogo from "../../../img/header_img/logo_big.png";
import Navigation from "./Navigation";

export function DesktopHeader() {
  return (
    <div className={deskHeader.container}>
      <div className={deskHeader.body}>
        <div className={deskHeader.topSide}>
          <div className={deskHeader.logoImg}>
            <a href="">
              <img src={headerLogo} alt="logo" />
            </a>
          </div>
          <div className={deskHeader.deliveryBlock}>
            <div className={deskHeader.deliveryTitleWrap}>
              <p className={deskHeader.deliveryTitle}>
                Доставка пиццы{" "}
                <span className={deskHeader.textOrange}>Москва</span>
              </p>
            </div>
            <div className={deskHeader.rateBlock}>
              <p className={deskHeader.deliveryTime}>34 мин</p>
              <img className={deskHeader.dot} src="img/ellipse.png" alt="" />
              <p className={deskHeader.rateCount}>4.72</p>
              <img
                className={deskHeader.rateStar}
                src="img/star.png"
                width="16"
                height="17"
                alt=""
              />
            </div>
          </div>
          <div className={deskHeader.contactBlock}>
            <p className={deskHeader.phoneNumber}>8 800 302-00-60</p>
            <p className={deskHeader.phoneText}>Звонок бесплатный</p>
          </div>
          {/*<button className="do-btn__authority" type="submit"><a href="#do-popup__login">Войти</a></button>*/}
        </div>
        <Navigation />
      </div>
    </div>
  );
}
