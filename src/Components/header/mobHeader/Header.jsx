import React from "react";
import Menu from "../burgerMenu/Menu";
import head from "./header.module.css";
import { DesktopHeader } from "../desktopHeader/DesktopHeader";

function Header() {
  return (
    <header className={head.container}>
      <div className={head.block}>
        <div className={head.mobHeader}>
          <a className={head.mobLogo}></a>
          <button className={head.burgerMenuBtn}>
            <span className={head.spanTop}></span>
            <span className={head.spanBottom}></span>
          </button>
          <Menu />
        </div>
        <DesktopHeader />
      </div>
    </header>
  );
}

export default Header;
