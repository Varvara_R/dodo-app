import React from "react";
import SimpleBannerSlider from "../notInUse/SimpleBannerSlider";
import NewPopularSlider from "./sliders/sliderOfNew/NewPopularSlider";
import MainMenu from "./menu/MainMenu";
import DeliverySection from "./deliverySection/DeliverySection";
import PizzaSection from "./productSections/PizzaSection";
import ComboSection from "./productSections/ComboSection";
import DessertSection from "./productSections/DessertSection";
import AppetizersSection from "./productSections/AppetizersSection";
import BeveragesSection from "./productSections/BeveragesSection";
import { banners } from "../../banners";
import slideData from "../../siderData";
import Slider from "./sliders/bannerSlider/Slider";
import ShopCartSymbol from "./shopCart/shopCartSection/shopCartSymbolMobile/ShopCartSymbol";

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.name = props.name;
  }

  render() {
    return (
      <main className="do-main__container">
        <Slider slides={banners} />
        <p className="do-title">Новое и популярное</p>
        <NewPopularSlider slides={slideData} />
        <MainMenu />
        <PizzaSection />
        {/*<ComboSection />*/}
        <AppetizersSection />
        <DessertSection />
        <BeveragesSection />
        <DeliverySection />
        <ShopCartSymbol />
      </main>
    );
  }
}
export default Main;
