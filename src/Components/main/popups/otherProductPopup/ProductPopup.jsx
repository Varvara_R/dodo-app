import React from "react";
import { connect } from "react-redux";
import popup from "./productPopup.module.css";

import arrow from "../../../../img/vectors/close_arrow.svg";
import cesar from "../../../../img/cezar/cezar-traditional-medium.jpeg";

import EnergyPopup from "../energyPopup/EnergyPopup";
import { addToBasket } from "../../../../redux/actions";

class ProductPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props.product,
      energyShown: false,
    };
    this.createInfo = this.createInfo.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
  }
  createInfo() {
    this.setState({ energyShown: !this.state.energyShown });
  }
  submitHandler = () => {
    const newProduct = {
      title: this.state.title,
      id: this.state.id,
      productPrice: this.state.price,
      quantity: 1,
      createIdentifier: this.state.createIdentifier,
      volume: this.state.volume,
    };
    console.log(newProduct);
    this.props.addToBasket(newProduct);
    this.props.onHide();
  };
  render() {
    // console.log(this.state);
    return (
      <div className={popup.display}>
        <a href="#" className={popup.area} onClick={this.props.onHide}></a>
        <div className={popup.body}>
          <div className={popup.content}>
            <a href="#" className={popup.closeElem} onClick={this.props.onHide}>
              +
            </a>
            <img
              src={arrow}
              className={popup.closeArrow}
              onClick={this.props.onHide}
              alt="arrow"
            />
            <div className={popup.leftSide}>
              <img className={popup.img} src={cesar} alt="pizza" />
            </div>
            <div className={popup.rightSide}>
              <EnergyPopup
                showEnergy={this.createInfo}
                energyShown={this.state.energyShown}
                pizza={this.props.product}
              />
              <h3 className={popup.title}>{this.state.title}</h3>
              <p className={popup.text}>{this.state.text}</p>
              <p className={popup.text}>{this.state.volume}</p>
              <p className={popup.description}>{this.state.description}</p>
              <div className={popup.footer}>
                <button
                  className={popup.btnToShopCart}
                  onClick={this.submitHandler}
                >
                  Добавить в корзину за {this.state.price} ₽
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = {
  addToBasket: addToBasket,
};
export default connect(null, mapDispatchToProps)(ProductPopup);
