import energy from "./energyPopup.module.css";
import infoIcon from "../../../../img/vectors/info_i.svg";
// import ReactDom from "react-dom";

import React from "react";
import classNames from "classnames";

function EnergyPopup(props) {
  // console.log(props);
  let item = {};
  if (!props.pizza.products) return null;
  if (!props.dough) {
    item = props.pizza.products;
  }
  if (props.dough === "traditional") {
    if (props.size === "small") {
      item = props.pizza.products.traditional[0];
    } else if (props.size === "medium") {
      item = props.pizza.products.traditional[1];
    } else if (props.size === "big") {
      item = props.pizza.products.traditional[2];
    }
  } else if (props.dough === "thin") {
    if (props.size === "small") {
      item = props.pizza.products.traditional[0];
    } else if (props.size === "medium") {
      item = props.pizza.products.thin[0];
    } else if (props.size === "big") {
      item = props.pizza.products.thin[1];
    }
  }

  return (
    <button className={energy.infoBtn} onClick={props.showEnergy}>
      <img src={infoIcon} className={energy.infoIcon} />

      <div
        className={classNames(energy.box, [
          !props.energyShown ? "" : ` ${energy.show}`,
        ])}
      >
        <div className={energy.content}>
          <h3 className={energy.boxTitle}>Пищевая ценность на 100г</h3>
          {item.ccal ? (
            <p className={energy.boxElem}>
              Энерг.ценность
              <span className={energy.ccal}>{item.ccal} г</span>
            </p>
          ) : null}
          {item.protein ? (
            <p className={energy.boxElem}>
              Белки <span className={energy.proteins}>{item.protein} г</span>
            </p>
          ) : null}
          {item.fat ? (
            <p className={energy.boxElem}>
              Жиры <span className={energy.fats}>{item.fat} г</span>
            </p>
          ) : null}
          {item.carbs ? (
            <p className={energy.boxElem}>
              Углеводы
              <span className={energy.carbos}>{item.carbs} г</span>
            </p>
          ) : null}
          {item.weight ? (
            <p className={energy.boxElem}>
              Вес
              <span className={energy.weight}>{item.weight} г</span>
            </p>
          ) : null}
          {item.size ? (
            <p className={energy.boxElem}>
              Диаметр
              <span className={energy.size}>{item.size} см</span>
            </p>
          ) : null}
        </div>
      </div>
    </button>
  );
}
export default EnergyPopup;
