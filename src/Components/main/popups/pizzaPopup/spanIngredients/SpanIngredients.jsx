import styles from "./spanIngredients.module.css";
import delIcon from "../../../../../img/vectors/delete.svg";
import returnIcon from "../../../../../img/vectors/back.svg";
import React from "react";
import classNames from "classnames";

class SpanIngredients extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      exclIngredients: [],
      isDeleted: false,
    };
    this.deleteIngredient = this.deleteIngredient.bind(this);
  }
  deleteIngredient() {
    this.setState({ isDeleted: !this.state.isDeleted });
  }

  render() {
    // console.log(this.props);
    return (
      <>
        <button className={classNames(styles.box)}>
          <span
            className={
              this.props.isIncluded
                ? this.state.isDeleted
                  ? styles.lineTrough
                  : styles.dashed
                : ""
            }
          >
            {this.props.title}
          </span>
          {this.props.isIncluded ? (
            <img
              src={this.state.isDeleted ? returnIcon : delIcon}
              className={styles.icon}
              onClick={() => {
                {
                  this.props.checkIngredients(this.props.title);
                  this.deleteIngredient();
                }
              }}
            />
          ) : null}
          {",  "}
        </button>
      </>
    );
  }
}
export default SpanIngredients;
