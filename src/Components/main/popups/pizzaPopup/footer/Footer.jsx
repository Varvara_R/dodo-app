import styles from "../pizzaPopupBody/pizzaPopup.module.css";
import React from "react";

class Footer extends React.Component {
  constructor(props) {
    super(props);

    // this.handleProductPriceChange = this.handleProductPriceChange.bind(this);
  }
  // handleProductPriceChange() {
  //   if (this.props.size === "small") {
  //     this.setState({ productPrice: this.props.data.pizzaSize[0].price });
  //   } else if (this.props.size === "medium") {
  //     this.setState({ productPrice: this.props.data.pizzaSize[1].price });
  //   } else if (this.props.size === "medium") {
  //     this.setState({ productPrice: this.props.data.pizzaSize[2].price });
  //   }
  // }
  render() {
    // console.log(this.props);

    return (
      <div className={styles.footer}>
        <button
          className={styles.btnToCart}
          type="submit"
          // onSubmit={this.props.submitHandler}
          onClick={this.props.submitHandler}
        >
          {!this.props.productPrice
            ? null
            : `Добавить в корзину за ${
                this.props.productPrice + this.props.addedPrice
              } ₽`}
        </button>
      </div>
    );
  }
}
export default Footer;
