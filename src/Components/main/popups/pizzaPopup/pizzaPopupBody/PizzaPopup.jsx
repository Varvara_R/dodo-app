import React from "react";
import { connect } from "react-redux";

import pizzaPopup from "./pizzaPopup.module.css";
import cesar from "../../../../../cezar-traditional-medium.jpeg";
import arrow from "../../../../../img/vectors/close_arrow.svg";
import { Description } from "../description/Description";
import SpanIngredients from "../spanIngredients/SpanIngredients";
import { ToggleBtns } from "../btns/ToggleBtns";
import Topping from "../toppings/Topping";
import EnergyPopup from "../../energyPopup/EnergyPopup";
import Footer from "../footer/Footer";
import { addToBasket } from "../../../../../redux/actions";

class PizzaPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props.pizza,
      size: "medium",
      dough: "traditional",
      toppings: [],
      exlcIngredients: [],
      quantity: 1,
      energyShown: false,
      productPrice: props.pizza.pizzaSize[1].price,
      addedPrice: 0,
    };
    this.changePizzaSize = this.changePizzaSize.bind(this);
    this.changeDough = this.changeDough.bind(this);
    this.createInfo = this.createInfo.bind(this);
    this.handleProductPriceChange = this.handleProductPriceChange.bind(this);
    this.checkIngredients = this.checkIngredients.bind(this);
    this.checkToppings = this.checkToppings.bind(this);
    this.recalcAddedPrice = this.recalcAddedPrice.bind(this);
    // this.createIdentifier = this.createIdentifier.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
  }
  componentDidMount() {
    let newArray = [];
    this.props.pizza.additionalIngredients.map((elem) => {
      let toppingObj = {
        isActive: false,
        toppingId: elem.id,
        title: elem.title,
      };
      newArray.push(toppingObj);
      this.setState({ toppings: newArray });
      // console.log(this.state.toppings);
    });

    let ingredientsArray = [];
    this.props.pizza.ingredients.map((elem) => {
      if (!elem.isIncluded) return;
      let ingredObj = { isExcluded: false, title: elem.title };
      ingredientsArray.push(ingredObj);
      this.setState({ exlcIngredients: ingredientsArray });
      // console.log(ingredientsArray);
    });
  }

  changePizzaSize(event) {
    this.setState({ size: event.target.value });
    if (event.target.value === "small") {
      this.setState({ dough: "traditional" });
    }
    console.log(this.state.size);
    this.handleProductPriceChange(event);
    this.recalcAddedPrice();
  }

  changeDough(event) {
    this.setState({ dough: event.target.value });
  }

  createInfo() {
    this.setState({ energyShown: !this.state.energyShown });
  }

  handleProductPriceChange(event) {
    if (event.target.value === "small")
      this.setState({ productPrice: this.state.pizzaSize[0].price });
    if (event.target.value === "medium")
      this.setState({ productPrice: this.state.pizzaSize[1].price });
    if (event.target.value === "big")
      this.setState({ productPrice: this.state.pizzaSize[2].price });
  }

  checkIngredients = (title) => {
    let index = this.state.exlcIngredients.findIndex(
      (item) => item.title == title
    );
    this.state.exlcIngredients[index].isExcluded = !this.state.exlcIngredients[
      index
    ].isExcluded;
  };

  checkToppings = (id) => {
    let index = this.state.toppings.findIndex((item) => item.toppingId === +id);
    this.state.toppings[index].isActive = !this.state.toppings[index].isActive;
    if (this.state.size === "small" || this.state.dough === "thin") {
      this.state.toppings[0].isActive = false;
    }
    this.recalcAddedPrice();
  };
  recalcAddedPrice() {
    let arrayOfActive = this.state.toppings.filter(
      (item) => item.isActive === true
    );
    let toppingsArray = this.props.pizza.additionalIngredients;
    let filteredArray = toppingsArray.filter(function (el) {
      return arrayOfActive.some(function (el2) {
        return el.id === el2.toppingId;
      });
    });
    // console.log(filteredArray);
    let result = filteredArray
      .map((elem) => elem.price[this.state.size])
      .reduce(function (total, amount) {
        return total + amount;
      }, 0);
    console.log(result);
    this.setState({ addedPrice: result });
  }

  // createIdentifier() {
  //   let filteredToppings = this.state.toppings.filter(
  //     (item) => item.isActive === true
  //   );
  //   let arrayOfExcluded = this.state.exlcIngredients.filter(
  //     (item) => item.isExcluded === true
  //   );
  //   // console.log(arrayOfExcluded);
  //   let identifier =
  //     this.state.id +
  //     this.state.size.charAt(0) +
  //     this.state.dough.substring(0, 2) +
  //     filteredToppings.map((elem) => elem.toppingId) +
  //     arrayOfExcluded.map((elem) => elem.title.substring(0, 2));
  //   this.setState({ createIdentifier: identifier });
  //   console.log(identifier);
  // }
  submitHandler = () => {
    // event.preventDefault();
    // this.createIdentifier();
    let filteredToppings = this.state.toppings.filter(
      (item) => item.isActive === true
    );
    let arrayOfExcluded = this.state.exlcIngredients.filter(
      (item) => item.isExcluded === true
    );
    const newProduct = {
      title: this.state.title,
      id: this.state.id,
      productPrice: this.state.productPrice,
      addedPrice: this.state.addedPrice,
      exclIngredients: arrayOfExcluded,
      toppings: filteredToppings,
      quantity: 1,
      size: this.state.size,
      dough: this.state.dough,
      createIdentifier:
        this.state.id +
        this.state.size.charAt(0) +
        this.state.dough.substring(0, 2) +
        filteredToppings.map((elem) => elem.toppingId) +
        arrayOfExcluded.map((elem) => elem.title.substring(0, 2)),
    };
    console.log(newProduct);
    this.props.addToBasket(newProduct);
    this.props.onHide();
  };

  // this.state.toppings[index].isActive = !this.state.toppings[index].isActive;
  // this.setState({
  //   toppings: [
  //     ...this.state.toppings[index],
  //     isActive: !this.state.toppings[index].isActive,
  //   ],
  // });
  // console.log(this.state.toppings[index]);

  render() {
    // console.log(this.props);
    return (
      <div className={pizzaPopup.display}>
        <a
          href="#do__pizza"
          className={pizzaPopup.area}
          onClick={this.props.onHide}
        ></a>
        <div className={pizzaPopup.body}>
          <div className={pizzaPopup.content}>
            <a
              href="#"
              className={pizzaPopup.closeElem}
              onClick={this.props.onHide}
            >
              +
            </a>
            <img
              src={arrow}
              className={pizzaPopup.closeArrow}
              onClick={this.props.onHide}
            />
            <div className={pizzaPopup.leftSide}>
              <div className={pizzaPopup.bigRound}>
                <div className={pizzaPopup.mediumRound}>
                  <img className={pizzaPopup.img} src={cesar} alt="" />
                </div>
              </div>
            </div>
            <div className={pizzaPopup.rightSide}>
              <EnergyPopup
                showEnergy={this.createInfo}
                energyShown={this.state.energyShown}
                pizza={this.props.pizza}
                size={this.state.size}
                dough={this.state.dough}
              />
              <p className={pizzaPopup.title}>{this.props.pizza.title}</p>
              <Description
                data={this.props.pizza.description}
                dough={this.state.dough}
                size={this.state.size}
              />
              <div className={pizzaPopup.ingredients}>
                {!this.props.pizza.ingredients
                  ? null
                  : this.props.pizza.ingredients.map((elem) => (
                      <SpanIngredients
                        title={elem.title}
                        isIncluded={elem.isIncluded}
                        checkIngredients={this.checkIngredients}
                      />
                    ))}
              </div>
              <div className={pizzaPopup.btnsContainer}>
                <ToggleBtns
                  changePizzaSize={this.changePizzaSize}
                  changeDough={this.changeDough}
                  size={this.state.size}
                  dough={this.state.dough}
                  // handleDescription={this.props.handleDescription}
                />
              </div>
              <h3 className={pizzaPopup.toppingsTitle}>Добавить в пиццу</h3>

              {!this.props.pizza.additionalIngredients ? null : (
                <div className={pizzaPopup.toppingsContainer}>
                  {this.props.pizza.additionalIngredients.map((elem) => (
                    <Topping
                      topping={elem}
                      key={elem.id}
                      id={elem.id}
                      title={elem.title}
                      img={elem.img}
                      isActive={elem.isActive}
                      price={elem.price[this.state.size]}
                      // toppings={this.props.product.toppings}
                      pizzaSize={this.state.size}
                      dough={this.state.dough}
                      checkToppings={this.checkToppings}
                    />
                  ))}
                </div>
              )}

              <Footer
                productPrice={this.state.productPrice}
                addedPrice={this.state.addedPrice}
                createIdentifier={this.createIdentifier}
                submitHandler={this.submitHandler}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = {
  addToBasket: addToBasket,
};
export default connect(null, mapDispatchToProps)(PizzaPopup);
