import toggleBtns from "./toggleBtn.module.css";
export function ToggleBtns(props) {
  // console.log(props);
  let sizeStyle;
  let doughStyle;
  if (props.size === "small") {
    sizeStyle = { transform: `translateX(calc(0%)` };
  }
  if (props.size === "medium") {
    sizeStyle = { transform: `translateX(calc(100%)` };
  }
  if (props.size === "big") {
    sizeStyle = { transform: `translateX(calc(200%)` };
  }
  if (props.dough === "traditional") {
    doughStyle = { transform: `translateX(calc(0%)` };
  }
  if (props.dough === "thin") {
    doughStyle = { transform: `translateX(calc(95%)` };
  }
  return (
    <>
      <div className={toggleBtns.sizeSwitch}>
        <div className={toggleBtns.sizeToggle} style={sizeStyle}></div>
        <input
          type="radio"
          id="small"
          name="pizzaSize"
          value="small"
          className={toggleBtns.sizeInput}
          onClick={props.changePizzaSize}
        />
        <label htmlFor="small" value="small" className={toggleBtns.sizeLabel}>
          Маленькая
        </label>
        <input
          type="radio"
          id="medium"
          name="pizzaSize"
          value="medium"
          className={toggleBtns.sizeInput}
          onClick={props.changePizzaSize}
        />
        <label htmlFor="medium" value="medium" className={toggleBtns.sizeLabel}>
          Средняя
        </label>
        <input
          type="radio"
          id="big"
          name="pizzaSize"
          value="big"
          className={toggleBtns.sizeInput}
          onClick={props.changePizzaSize}
        />
        <label htmlFor="big" value="big" className={toggleBtns.sizeLabel}>
          Большая
        </label>
      </div>
      <div className={toggleBtns.doughSwitch}>
        <div className={toggleBtns.doughToggle} style={doughStyle}></div>
        <input
          type="radio"
          id="traditional"
          value="traditional"
          name="dough"
          className={toggleBtns.doughInput}
          onClick={props.changeDough}
        />
        <label
          htmlFor="traditional"
          value="traditional"
          className={toggleBtns.doughTraditional}
        >
          Традиционное
        </label>
        <input
          type="radio"
          id="thin"
          value="thin"
          name="dough"
          className={toggleBtns.doughInput}
          onClick={props.changeDough}
          disabled={props.size === "small" ? true : false}
        />
        <label
          htmlFor="thin"
          value="thin"
          className={toggleBtns.doughThin}
          style={props.size === "small" ? { opacity: "0.5" } : { opacity: "1" }}
        >
          Тонкое
        </label>
      </div>
    </>
  );
}
