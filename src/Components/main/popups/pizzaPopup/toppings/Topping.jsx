import toppings from "./toppings.module.css";
import React from "react";
import classNames from "classnames";

class Topping extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props,
      isRed: false,
    };
    this.makeRedBorder = this.makeRedBorder.bind(this);
    // this.removeRedBorder = this.removeRedBorder.bind(this);
  }
  makeRedBorder() {
    this.setState({ isRed: !this.state.isRed });
    // this.props.checkToppings(this.props.id);
    // console.log("upd");
  }

  render() {
    return (
      <button
        id={this.props.id}
        className={classNames(
          toppings.btn,
          !this.state.isRed
            ? ""
            : (this.props.pizzaSize === "small" ||
                this.props.dough === "thin") &&
              this.props.id === 1
            ? ""
            : toppings.redBorder
        )}
        // onClick={this.makeRedBorder}
        onClick={() => {
          {
            this.props.checkToppings(this.props.id);
            this.makeRedBorder();
          }
        }}
        disabled={
          (this.props.pizzaSize === "small" || this.props.dough === "thin") &&
          this.props.id === 1
            ? true
            : false
        }
      >
        <img
          className={classNames(
            toppings.img,
            (this.props.pizzaSize === "small" || this.props.dough === "thin") &&
              this.props.id === 1
              ? toppings.opacityDisable
              : ""
          )}
          src={this.props.img}
        />
        <h6 className={toppings.title}>{this.props.title}</h6>
        <p className={toppings.price}>{this.props.price}</p>
      </button>
    );
  }
}
export default Topping;
