import pizzaPopup from "../pizzaPopupBody/pizzaPopup.module.css";

export function Description(props) {
  // console.log(props);
  let item = {};
  if (!props.data) return null;
  if (props.dough === "traditional") {
    if (props.size === "small") {
      item = props.data.traditional[0];
    } else if (props.size === "medium") {
      item = props.data.traditional[1];
    } else if (props.size === "big") {
      item = props.data.traditional[2];
    }
  } else if (props.dough === "thin") {
    if (props.size === "small") {
      item = props.data.traditional[0];
    } else if (props.size === "medium") {
      item = props.data.thin[0];
    } else if (props.size === "big") {
      item = props.data.thin[1];
    }
  }

  return (
    <>
      <p className={pizzaPopup.description}>
        {!item
          ? null
          : `${item.sm} cм, 
              ${item.dough}, 
              ${item.weight} гр.`}
      </p>
    </>
  );
}
