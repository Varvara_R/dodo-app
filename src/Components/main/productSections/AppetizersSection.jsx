import section from "./css/productsSections.module.css";
import ProductCard from "../productsCards/OtherProducts";
import { appetizers } from "../../../appetizers";

function AppetizersSection() {
  return (
    <>
      <div id="do__appetizers" className={section.title}>
        <p className={section.title}>Закуски</p>
      </div>
      <section className={section.section}>
        <ProductCard data={appetizers} />
      </section>
    </>
  );
}
export default AppetizersSection;
