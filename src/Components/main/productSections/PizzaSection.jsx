import section from "./css/productsSections.module.css";
import React from "react";
import PizzaCard from "../productsCards/PizzaCards";
import { productsPizzas } from "../../../products";
import PizzaPopup from "../popups/pizzaPopup/pizzaPopupBody/PizzaPopup";

// const data = {
//   traditional: {
//     small: {
//       size: 25,
//       sizeSymbol: "см",
//       doughTitle: "традиционное тесто",
//       weight: 440,
//       weightSymbol: "г",
//     },
//   },
// };

class PizzaSection extends React.Component {
  constructor(props) {
    super(props);
  }
  // this.getDough = this.getDough.bind(this);
  // this.handlePriceChange = this.handlePriceChange.bind(this);
  // this.handleDescriptionChange = this.handleDescriptionChange.bind(this);

  // getDough(event) {
  //   this.setState({ dough: event.target.value });
  //   // console.log(event.target.value);
  //   if (this.state.size === "small") {
  //      this.setState({ dough: "traditional" });
  //   }
  // }
  // handleDescriptionChange() {
  //   if (this.state.size === "big") {
  //     return `${this.state.pizza.pizzaSize[2].size} cм, ${this.state.pizza.dough[0]}, ${this.state.pizza.pizzaSize[2].weight.traditional}г`;
  //   }
  // }

  render() {
    return (
      <>
        <div id="do__pizza" className={section.title}>
          <p>Пицца</p>
        </div>
        <section className={section.section}>
          <PizzaCard data={productsPizzas} />
        </section>
      </>
    );
  }
}
export default PizzaSection;
