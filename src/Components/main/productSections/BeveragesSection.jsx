import section from "./css/productsSections.module.css";
import ProductCard from "../productsCards/OtherProducts";
import { beverages } from "../../../beverages";

function BeveragesSection() {
  return (
    <>
      <div id="do__beverages" className={section.title}>
        <p className={section.title}>Напитки</p>
      </div>
      <section className={section.section}>
        <ProductCard data={beverages} />
      </section>
    </>
  );
}
export default BeveragesSection;
