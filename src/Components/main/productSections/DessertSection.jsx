import section from "./css/productsSections.module.css";
import ProductCard from "../productsCards/OtherProducts";
import { desserts } from "../../../desserts";

function DessertSection() {
  return (
    <>
      <div id="do__desserts" className={section.title}>
        <p className={section.title}>Десерты</p>
      </div>
      <section className={section.section}>
        <ProductCard data={desserts} />
      </section>
    </>
  );
}
export default DessertSection;
