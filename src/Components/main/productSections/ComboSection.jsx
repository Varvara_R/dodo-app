import section from "./css/productsSections.module.css";
import ProductCard from "../productsCards/OtherProducts";
import { combos } from "../../../combos";

function ComboSection() {
  return (
    <>
      <div id="do__combo" className={section.title}>
        <p className={section.title}>Комбо</p>
      </div>
      <section className={section.section}>
        {combos.map((combo) => (
          <ProductCard
            key={combo.id}
            id={combo.id}
            title={combo.title}
            description={combo.description}
            price={combo.price}
          />
        ))}
      </section>
    </>
  );
}
export default ComboSection;
