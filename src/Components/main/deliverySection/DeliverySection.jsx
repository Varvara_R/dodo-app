import React from "react";
import delivery from "./delivery.module.css";

class DeliverySection extends React.Component {
  render() {
    return (
      <div>
        <div id="delivery_and_pay" className={delivery.title}>
          <p>Доставка и оплата</p>
        </div>
        <div className={delivery.container}>
          <div className={delivery.article}>
            <h4 className={delivery.articleTitle}>
              60 МИНУТ ИЛИ ПИЦЦА БЕСПЛАТНО
            </h4>
            <p className={delivery.articleContent}>
              Если мы не успеем доставить продукты в течение 60 минут, кроме
              соусов и продуктов из раздела «Другие товары», курьер подарит вам
              сертификат или промокод на большую пиццу 35 см на традиционном
              тесте.
            </p>
          </div>
          <div className={delivery.article}>
            <h4 className={delivery.articleTitle}>
              ДОДО ПИЦЦА — СЕТЬ ПИЦЦЕРИЙ № 1 В РОССИИ
            </h4>
            <p className={delivery.articleContent}>
              по доставке пиццы по данным из отчета Euromonitor International на
              май 2017 года.
              <br />
              Все цены в меню указаны без учета скидок
            </p>
          </div>
          <div className={delivery.verticalArticle}>
            <div className={delivery.articleExtended}>
              <h4 className={delivery.articleTitle}>495 ₽</h4>
              <p className={delivery.articleContent}>
                Минимальная сумма доставки без учета товаров из категории
                «Другие товары»
              </p>
            </div>
            <div className={delivery.article}>
              <h4 className={delivery.articleTitle}>7000 ₽</h4>
              <p className={delivery.articleContent}>
                Максимальная сумма при оплате наличн
                <br />
                Изображения продуктов могут отличаться от продуктов в заказе.
              </p>
            </div>
          </div>
          <div className={delivery.article}>
            <h4 className={delivery.articleTitle}>ЗОНА ДОСТАВКИ ОГРАНИЧЕНА</h4>
            <div className={delivery.articleContent}>
              <a href="">
                <img
                  src="img/delivery_map.png"
                  height="290"
                  width="250"
                  alt=""
                />
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default DeliverySection;
