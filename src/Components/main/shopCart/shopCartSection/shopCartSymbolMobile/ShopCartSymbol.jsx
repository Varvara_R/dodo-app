import React from "react";
import { connect } from "react-redux";
import styles from "./symbolStyles.module.css";
import shopCartImg from "../../../../../img/vectors/shop_cart_mob.png";
import ShopCartPopup from "../shopCartPopup/1popupBody/ShopCartPopup";

class ShopCartSymbol extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    };
    this.openModal = this.openModal.bind(this);
    this.onHide = this.onHide.bind(this);
  }
  openModal() {
    this.setState({ showModal: true });
  }
  onHide() {
    this.setState({ showModal: false });
  }
  render() {
    return (
      <>
        {this.props.syncProducts.length > 0 && (
          <div className={styles.container}>
            <div className={styles.body}>
              <div className={styles.content}>
                <img
                  className={styles.img}
                  src={shopCartImg}
                  onClick={() => this.openModal()}
                />
                <div className={styles.counterContainer}>
                  <span className={styles.number}>
                    {this.props.syncProducts.length}
                  </span>
                </div>
              </div>
            </div>
          </div>
        )}
        {this.state.showModal && <ShopCartPopup onHide={this.onHide} />}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    syncProducts: state.products,
  };
};

export default connect(mapStateToProps, null)(ShopCartSymbol);
