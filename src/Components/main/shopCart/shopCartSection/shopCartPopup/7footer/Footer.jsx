import styles from "./footer.module.css";
import back from "../../../../../../img/vectors/gray_back.svg";
import forward from "../../../../../../img/vectors/forward.svg";

export function Footer(props) {
  return (
    <>
      <h2 className={styles.headline}>
        Стоимость <span className={styles.amount}>{props.price}</span>
      </h2>
      <div className={styles.btnsContainer}>
        <button className={styles.backBtn}>
          <img className={styles.backBtnIcon} src={back} alt="back" /> Вернуться
          назад
        </button>
        <button className={styles.btnToCart}>
          <h3 className={styles.btnToCartText}>К оформлению заказа</h3>
          <img
            className={styles.btnForwardIcon}
            src={forward}
            alt="go forward"
          />
        </button>
      </div>
    </>
  );
}
