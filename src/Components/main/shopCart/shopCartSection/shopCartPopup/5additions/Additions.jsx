import styles from "./additions.module.css";
import { SliderProduct } from "./sliderProduct/sliderProduct";
import { desserts } from "../../../../../../desserts";
import { beverages } from "../../../../../../beverages";
import { appetizers } from "../../../../../../appetizers";

let offers = [desserts[2], appetizers[2], beverages[2]];

export function Additions(props) {
  return (
    <div className={styles.container}>
      <h3 className={styles.title}>{props.title}</h3>
      <div className={styles.slider}>
        <SliderProduct data={offers} />
      </div>
    </div>
  );
}
