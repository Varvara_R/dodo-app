import styles from "./sliderProduct.module.css";
import cesar from "../../../../../../../img/cezar/cezar-traditional-medium.jpeg";

export function SliderProduct({ data }) {
  console.log(data);
  return (
    <>
      {data.map((offer) => (
        <div className={styles.container} key={offer.id} id={offer.id}>
          <div className={styles.leftSide}>
            <img className={styles.img} src={cesar} alt="offerImg" />
          </div>
          <div className={styles.rightSide}>
            <h3 className={styles.title}>{offer.title}</h3>
            <p className={styles.price}>{offer.price}</p>
          </div>
        </div>
      ))}
    </>
  );
}
