import React from "react";
import ReactDom from "react-dom";
import styles from "./shopCart.module.css";
import { HeaderShopClass } from "../2headerShopCart/HeaderShopCart";
import Product from "../3productsContainer/Product";
import GiftsItems from "../4giftsItems/GiftsItems";
import { Additions } from "../5additions/Additions";
import { Promo } from "../6promo/promo";
import { Footer } from "../7footer/Footer";

function ShopCartPopup(props) {
  return ReactDom.createPortal(
    <div className={styles.container}>
      <a href="#" className={styles.area} onClick={props.onHide}></a>
      <div className={styles.body}>
        <div className={styles.content}>
          <a href="#" className={styles.closeElem} onClick={props.onHide}>
            +
          </a>
          <HeaderShopClass />
          <div className={styles.titleContainer}>
            <h3 className={styles.title}>
              В корзине: <span className={styles.titleQty}></span>
            </h3>
          </div>
          <div className={styles.productsContainer}>
            <Product />
          </div>

          <div className={styles.space}></div>
          <h3 className={styles.giftTitle}>Подарки</h3>

          <GiftsItems />

          <Additions title="Добавить к заказу" />

          <Promo title="Промокод" btnName="Применить" />

          <Footer />
        </div>
      </div>
    </div>,
    document.getElementById("shop-cart")
  );
}

export default ShopCartPopup;
