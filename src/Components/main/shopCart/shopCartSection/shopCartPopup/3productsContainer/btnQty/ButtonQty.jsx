import styles from "./buttonQty.module.css";
import React from "react";
import classNames from "classnames";

const ButtonQty = (props) => {
  return (
    <div className={styles.toggle}>
      <button className={classNames(styles.btn, styles.minusBtn)}>-</button>
      <input className={styles.qty} type="text" />
      {props.quantity}

      <button className={classNames(styles.btn, styles.plusBtn)}>+</button>
    </div>
  );
};
export default ButtonQty;
