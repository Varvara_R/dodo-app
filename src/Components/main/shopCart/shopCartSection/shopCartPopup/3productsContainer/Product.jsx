import styles from "./product.module.css";
import React from "react";
import { connect } from "react-redux";

import bin from "../../../../../../img/vectors/bin.svg";
import cezar from "../../../../../../img/cezar/cezar-traditional-medium.jpeg";
import ButtonQty from "./btnQty/ButtonQty";

function Product({ syncProducts }) {
  if (!syncProducts.length) {
    return <p>Добавьте что-нибудь в корзину</p>;
  }
  return (
    <div className={styles.box}>
      {syncProducts.map((elem) => (
        <div className={styles.element} key={elem.id}>
          <img className={styles.deleteImg} src={bin} />
          <div className={styles.topSide}>
            <div className={styles.topLeftSide}>
              <img className={styles.productImg} src={cezar} />
            </div>
            <div className={styles.topRightSide}>
              <h3 className={styles.productTitle}>{elem.title}</h3>
              {elem.toppings ? (
                <p className={styles.productToppings}>
                  + {elem.toppings.map((item) => item.title + " ")}
                </p>
              ) : (
                ""
              )}
              {elem.exclIngredients ? (
                <p className={styles.productExclIngredients}>
                  - {elem.exclIngredients.map((item) => item.title + " ")}
                </p>
              ) : (
                ""
              )}
              <p className={styles.productToppings}>
                {elem.size === "small"
                  ? "Маленькая 25см"
                  : elem.size === "medium"
                  ? "Средняя 30см"
                  : elem.size === "big"
                  ? "Большая 35см"
                  : elem.volume}
              </p>
              {elem.dough ? (
                <p className={styles.productToppings}>
                  {elem.dough === "traditional"
                    ? "Традиционное тесто"
                    : elem.dough === "thin"
                    ? "Тонкое тесто"
                    : ""}
                </p>
              ) : (
                ""
              )}
            </div>
          </div>
          <div className={styles.bottomSide}>
            <p className={styles.price}>
              {elem.addedPrice
                ? elem.productPrice + elem.addedPrice
                : elem.productPrice}{" "}
              ₽
            </p>
            <ButtonQty quantity={elem.quantity} />
          </div>
        </div>
      ))}
    </div>
  );
}
const mapStateToProps = (state) => {
  console.log(state);
  return {
    syncProducts: state.products,
  };
};

export default connect(mapStateToProps, null)(Product);
