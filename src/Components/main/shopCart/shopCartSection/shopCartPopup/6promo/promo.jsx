import styles from "./promo.module.css";

export function Promo(props) {
  return (
    <div className={styles.container}>
      <h3 className={styles.title}>{props.title}</h3>
      <input
        className={styles.input}
        type="text"
        placeholder="Введите промокод"
      />
      <button className={styles.btn}>{props.btnName}</button>
    </div>
  );
}
