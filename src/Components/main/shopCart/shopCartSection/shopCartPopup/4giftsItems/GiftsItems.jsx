import styles from "./giftsItems.module.css";
import React from "react";
import giftSet from "../../../../../../img/shopCart/napkins.png";
import giftVector from "../../../../../../img/shopCart/gift.svg";
import ButtonQty from "../3productsContainer/btnQty/ButtonQty";

class GiftsItems extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className={styles.container}>
        <div className={styles.topSide}>
          <div className={styles.topLeft}>
            <img className={styles.img} alt="giftSet" src={giftSet} />
          </div>
          <div className={styles.topRight}>
            <h3 className={styles.title}>Додо набор</h3>
            <p className={styles.description}>1 шт.</p>
            <div className={styles.giftLine}>
              <img
                className={styles.giftVector}
                src={giftVector}
                alt="giftVector"
              />
              <p className={styles.giftText}>Подарок</p>
            </div>
          </div>
        </div>
        <div className={styles.bottom}>
          <div className={styles.price}>0 ₽</div>
          <ButtonQty />
        </div>
      </div>
    );
  }
}
export default GiftsItems;
