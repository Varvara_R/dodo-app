import sliderOfNew from "./newPopSlider.module.css";
import React from "react";

class ProductSlide extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { slide } = this.props;
    return (
      <div className={sliderOfNew.box}>
        <div className={sliderOfNew.leftSide}>
          <img
            src={slide.src}
            alt={slide.headline}
            className={sliderOfNew.img}
          />
        </div>
        <div className={sliderOfNew.rightSide}>
          <p className={sliderOfNew.title}>{slide.headline}</p>
          <p className={sliderOfNew.price}>{slide.price}</p>
        </div>
      </div>
    );
  }
}
export default ProductSlide;
