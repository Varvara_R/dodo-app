import sliderOfNew from "./newPopSlider.module.css";
// import slideData from "../../siderData";
import ProductSlide from "./ProductSlide";
import ProductSLiderControl from "./ProductSliderControl";
import React from "react";
import bannerSlider from "../bannerSlider/bannerSlider.module.css";

class NewPopularSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = { current: 0 };
    this.handlePreviousClick = this.handlePreviousClick.bind(this);
    this.handleNextClick = this.handleNextClick.bind(this);
  }
  handlePreviousClick() {
    const previous = this.state.current - 1;
    this.setState({
      current: previous < 0 ? this.props.slides.length - 1 : previous,
    });
  }
  handleNextClick() {
    const next = this.state.current + 1;
    this.setState({ current: next === this.props.slides.length ? 0 : next });
  }
  render() {
    const { current } = this.state;
    const { slides } = this.props;
    const wrapperTransform = {
      transform: `translateX(-${current * 250}px)`,
    };
    return (
      <div className={sliderOfNew.container}>
        <div className={sliderOfNew.slider} style={wrapperTransform}>
          {slides.map((slide) => (
            <ProductSlide slide={slide} />
          ))}
        </div>
        <div className={sliderOfNew.sliderControl}>
          <ProductSLiderControl
            type="previous"
            handleClick={this.handlePreviousClick}
          />
          <ProductSLiderControl
            type="next"
            handleClick={this.handleNextClick}
          />
        </div>
      </div>
    );
  }
}
export default NewPopularSlider;
