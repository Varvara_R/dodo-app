import sliderOfNew from "./newPopSlider.module.css";
import bannerSlider from "../bannerSlider/bannerSlider.module.css";
import classNames from "classnames";

const ProductSLiderControl = ({ type, handleClick }) => {
  return (
    <button
      className={classNames(
        sliderOfNew.btnOfNew,
        sliderOfNew[`btnOfNew${type}`]
      )}
      onClick={handleClick}
    >
      <svg className={sliderOfNew.icon} viewBox="0 0 24 24">
        <path d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" />
      </svg>
    </button>
  );
};
export default ProductSLiderControl;
