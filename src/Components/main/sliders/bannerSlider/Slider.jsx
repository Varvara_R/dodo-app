import bannerSlider from "./bannerSlider.module.css";
import React from "react";
import Slide from "./Slide";
import SliderControl from "./SliderControl";

class Slider extends React.Component {
  constructor(props) {
    super(props);
    this.state = { current: 0 };
    this.handlePreviousClick = this.handlePreviousClick.bind(this);
    this.handleNextClick = this.handleNextClick.bind(this);
    this.handleSlideClick = this.handleSlideClick.bind(this);
  }
  handlePreviousClick() {
    const previous = this.state.current - 1;
    this.setState({
      current: previous < 0 ? this.props.slides.length - 1 : previous,
    });
  }
  handleNextClick() {
    const next = this.state.current + 1;
    this.setState({ current: next === this.props.slides.length ? 0 : next });
  }
  handleSlideClick(index) {
    if (this.state.current !== index) {
      this.setState({ current: index });
    }
  }

  render() {
    const { current } = this.state;
    const { slides } = this.props;
    const wrapperTransform = {
      transform: `translateX(-${current * (100 / slides.length)}%)`,
    };
    return (
      <div className={bannerSlider.slider}>
        <ul className={bannerSlider.sliderWrapper} style={wrapperTransform}>
          {slides.map((slide) => {
            return (
              <Slide
                key={slide.index}
                slide={slide}
                current={current}
                handleSlideClick={this.handleSlideClick}
              />
            );
          })}
        </ul>
        <div className={bannerSlider.sliderControl}>
          <SliderControl
            type="previous"
            title="Go to previuos"
            handleClick={this.handlePreviousClick}
          />
          <SliderControl
            type="next"
            title="Go to next"
            handleClick={this.handleNextClick}
          />
        </div>
      </div>
    );
  }
}
export default Slider;
