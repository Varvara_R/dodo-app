import React from "react";
import cesar from "../../../cezar-traditional-medium.jpeg";
import styles from "./css/pizzaCard.module.css";
import ProductPopup from "../popups/otherProductPopup/ProductPopup";

class ProductCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      product: {},
    };
    this.onHide = this.onHide.bind(this);
    this.openModal = this.openModal.bind(this);
  }
  openModal(data) {
    this.setState({ showModal: true, product: data });
  }
  onHide = () => {
    this.setState({ showModal: false });
  };
  render() {
    // console.log(this.props.data);
    return (
      <>
        {this.props.data.map((product) => (
          <article className={styles.box} data-id={product.id}>
            <div className={styles.leftSide}>
              <img className={styles.img} src={cesar} />
            </div>
            <div className={styles.rightSide}>
              <h3 className={styles.title}>{product.title}</h3>
              <p className={styles.description}>{product.description}</p>
              <div className={styles.priceBox}>
                <button
                  className={styles.priceBtn}
                  id={product.id}
                  onClick={() => this.openModal(product)}
                >
                  <span className={styles.priceElem}>{product.price} ₽</span>
                </button>
              </div>
            </div>
          </article>
        ))}
        {this.state.showModal && (
          <ProductPopup onHide={this.onHide} product={this.state.product} />
        )}
      </>
    );
  }
}
export default ProductCard;
