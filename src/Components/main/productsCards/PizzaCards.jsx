import React from "react";
import cesar from "../../../cezar-traditional-medium.jpeg";
import pizzaCard from "./css/pizzaCard.module.css";
import PizzaPopup from "../popups/pizzaPopup/pizzaPopupBody/PizzaPopup";

class PizzaCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      pizza: {},
    };
    this.getModal = this.getModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }
  getModal = (data) => {
    this.setState({ showModal: true, pizza: data });
  };
  hideModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    return (
      <>
        {this.props.data.map((pizza, id) => (
          <article className={pizzaCard.box} data-id={pizza.id} key={id}>
            <div className={pizzaCard.leftSide}>
              <img className={pizzaCard.img} src={cesar} />
            </div>
            <div className={pizzaCard.rightSide}>
              <h3 className={pizzaCard.title}>{pizza.title}</h3>
              <p className={pizzaCard.description}>
                {pizza.ingredients.map((elem) => elem.title + ", ")}
              </p>
              <div className={pizzaCard.priceBox}>
                <button
                  className={pizzaCard.priceBtn}
                  onClick={() => this.getModal(pizza)}
                >
                  <span className={pizzaCard.priceElem}>
                    от {pizza.pizzaSize[0].price} ₽
                  </span>
                </button>
              </div>
            </div>
          </article>
        ))}
        {this.state.showModal && (
          <PizzaPopup onHide={this.hideModal} pizza={this.state.pizza} />
        )}
      </>
    );
  }
}
export default PizzaCard;
