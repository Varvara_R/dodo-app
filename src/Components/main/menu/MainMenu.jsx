import React from "react";
import menu from "./menu.module.css";

class MainMenu extends React.Component {
  render() {
    return (
      <div className={menu.container}>
        <button className={menu.btn} type="submit">
          <a href="#do__pizza" className={menu.link}>
            Пицца
          </a>
        </button>
        <button className={menu.btn} type="submit">
          <a href="#do__combo" className={menu.link}>
            Комбо
          </a>
        </button>
        <button className={menu.btn} type="submit">
          <a href="#do__appetizers" className={menu.link}>
            Закуски
          </a>
        </button>
        <button className={menu.btn} type="submit">
          <a href="#do__desserts" className={menu.link}>
            Десерты
          </a>
        </button>
        <button className={menu.btn} type="submit">
          <a href="#do__beverages" className={menu.link}>
            Напитки
          </a>
        </button>
      </div>
    );
  }
}
export default MainMenu;
