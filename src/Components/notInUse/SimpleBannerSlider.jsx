import React, { useState } from "react";
import bannerSlider from "./SimpleBannerSlider.module.css";
import { FaArrowAltCircleRight, FaArrowAltCircleLeft } from "react-icons/fa";
import { banners } from "../../banners";
import classNames from "classnames";

function SimpleBannerSlider() {
  const [current, setCurrent] = useState(0);
  const length = banners.length;

  const nextSlide = () => {
    setCurrent(current === length - 1 ? 0 : current + 1);
  };
  const prevSlide = () => {
    setCurrent(current === 0 ? length - 1 : current - 1);
  };
  console.log(current);
  return (
    <div className={bannerSlider.container}>
      <FaArrowAltCircleLeft className={bannerSlider.prev} onClick={prevSlide} />
      <FaArrowAltCircleRight
        className={bannerSlider.next}
        onClick={nextSlide}
      />
      {banners.map((item, index) => {
        return (
          <div
            className={
              index === current
                ? classNames(bannerSlider.slide, bannerSlider.active)
                : bannerSlider.slide
            }
            key={index}
          >
            {index === current && (
              <img
                src={item.image}
                alt="banner"
                className={bannerSlider.element}
              />
            )}
          </div>
        );
      })}
      <div className={bannerSlider.dotsContainer}>
        <span className={bannerSlider.dot}></span>
        <span className={bannerSlider.dot}></span>
        <span className={bannerSlider.dot}></span>
        <span className={bannerSlider.dot}></span>
        <span className={bannerSlider.dot}></span>
      </div>
    </div>
  );
}
export default SimpleBannerSlider;
