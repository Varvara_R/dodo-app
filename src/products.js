import blueCheese from "./img/toppingsImg/blue_cheese.png";
import brynza from "./img/toppingsImg/brynza.svg";
import cheddarParmezan from "./img/toppingsImg/cheddar_parmezan.png";
import cheeseBorder from "./img/toppingsImg/cheese_border.svg";
import chicken from "./img/toppingsImg/chicken.svg";
import chorizo from "./img/toppingsImg/chorizo.png";
import ham from "./img/toppingsImg/ham.png";
import mozarella from "./img/toppingsImg/mozarella.png";
import mushrooms from "./img/toppingsImg/mushrooms.svg";
import olives from "./img/toppingsImg/olives.png";
import redOnion from "./img/toppingsImg/red_onion.png";
import spicyChicken from "./img/toppingsImg/spicy_chiken.svg";
import tomatoes from "./img/toppingsImg/tomatos.png";
import halapenyo from "./img/toppingsImg/halapenyo.svg";

export let productsPizzas = [
  {
    id: 2,
    title: "Чиззи чеддер",
    description: {
      traditional: [
        {
          title: "small",
          sm: 25,
          dough: "традиционное тесто",
          weight: 330,
        },
        {
          title: "medium",
          sm: 30,
          dough: "традиционное тесто",
          weight: 480,
        },
        {
          title: "big",
          sm: 35,
          dough: "традиционное тесто",
          weight: 630,
        },
      ],
      thin: [
        {
          title: "medium",
          sm: 30,
          dough: "тонкое тесто",
          weight: 390,
        },
        {
          title: "big",
          sm: 35,
          dough: "тонкое тесто",
          weight: 530,
        },
      ],
    },
    pizzaSize: [
      {
        title: "Маленькая",
        size: 25,
        price: 395,
        weight: {
          thin: "",
          traditional: 330,
        },
        img: {
          thin: "",
          traditional: "img/cheesy-cheddar/cheesy-chadder-small.jpeg",
        },
      },
      {
        title: "Средняя",
        size: 30,
        price: 595,
        weight: {
          thin: 390,
          traditional: 480,
        },
        img: {
          thin: "img/cheesy-cheddar/cheesy-chadder-middle-thin.jpeg",
          traditional:
            "img/cheesy-cheddar/cheesy-chadder-middle-traditional.jpeg",
        },
      },
      {
        title: "Большая",
        size: 35,
        price: 745,
        weight: {
          thin: 530,
          traditional: 630,
        },
        img: {
          thin: "img/cheesy-cheddar/cheesy-chadder-big-thin.jpeg",
          traditional: "img/cheesy-cheddar/cheesy-chadder-big-traditional.jpeg",
        },
      },
    ],
    dough: ["традиционное тесто", "тонкое тесто"],
    additionalIngredients: [
      {
        id: 1,
        title: "Сырный бортик",
        price: {
          small: 169,
          medium: 169,
          big: 189,
        },
        img: cheeseBorder,
        isActive: true,
      },
      {
        id: 11,
        title: "Шампиньоны",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: mushrooms,
        isActive: true,
      },
      {
        id: 9,
        title: "Цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: chicken,
        isActive: true,
      },
      {
        id: 10,
        title: "Брынза",
        price: {
          small: 49,
          medium: 39,
          big: 49,
        },
        img: brynza,
        isActive: true,
      },
      {
        id: 2,
        title: "Острый халапеньо",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: halapenyo,
        isActive: true,
      },
      {
        id: 5,
        title: "Острый цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: spicyChicken,
        isActive: true,
      },
    ],
    products: {
      traditional: [
        {
          title: "small",
          ccal: 249.4,
          protein: 9.1,
          fat: 6.8,
          carbs: 36.2,
          weight: 330,
          size: 25,
        },
        {
          title: "medium",
          ccal: 253.4,
          protein: 9.4,
          fat: 7.1,
          carbs: 36.3,
          weight: 480,
          size: 30,
        },
        {
          title: "big",
          ccal: 252.4,
          protein: 9.2,
          fat: 6.8,
          carbs: 36.8,
          weight: 630,
          size: 35,
        },
      ],
      thin: [
        {
          title: "medium",
          ccal: 239.9,
          protein: 9.8,
          fat: 7.7,
          carbs: 31.2,
          weight: 390,
          size: 30,
        },
        {
          title: "big",
          ccal: 244.7,
          protein: 9.6,
          fat: 7.4,
          carbs: 33.4,
          weight: 530,
          size: 35,
        },
      ],
    },
    ingredients: [
      {
        title: "Ветчина",
        isIncluded: true,
      },
      {
        title: "сыр чеддер",
        isIncluded: false,
      },
      {
        title: "сладкий перец",
        isIncluded: true,
      },
      {
        title: "моцарелла",
        isIncluded: false,
      },
      {
        title: "томатный соус",
        isIncluded: false,
      },
      {
        title: "чеснок",
        isIncluded: true,
      },
      {
        title: "итальянские травы",
        isIncluded: true,
      },
    ],
  },
  {
    id: 3,
    title: "Цезарь",
    description: {
      traditional: [
        {
          title: "small",
          sm: 25,
          dough: "традиционное тесто",
          weight: 480,
        },
        {
          title: "medium",
          sm: 30,
          dough: "традиционное тесто",
          weight: 640,
        },
        {
          title: "big",
          sm: 35,
          dough: "традиционное тесто",
          weight: 870,
        },
      ],
      thin: [
        {
          title: "medium",
          sm: 30,
          dough: "тонкое тесто",
          weight: 530,
        },
        {
          title: "big",
          sm: 35,
          dough: "тонкое тесто",
          weight: 730,
        },
      ],
    },
    pizzaSize: [
      {
        title: "Маленькая",
        size: 25,
        weight: {
          thin: "",
          traditional: 480,
        },
        price: 445,
        img: {
          thin: "",
          traditional: "img/cezar/cezar-traditional-small.jpeg",
        },
      },
      {
        title: "Средняя",
        size: 30,
        price: 665,
        weight: {
          thin: 530,
          traditional: 640,
        },
        img: {
          thin: "img/cezar/cezar-thin-medium.jpeg",
          traditional: "img/cezar/cezar-traditional-medium.jpeg",
        },
      },
      {
        title: "Большая",
        size: 35,
        price: 785,
        weight: {
          thin: 730,
          traditional: 870,
        },
        img: {
          thin: "img/cezar/cezar-thin-big.jpeg",
          traditional: "img/cezar/cezar-traditional-big.jpeg",
        },
      },
    ],
    dough: ["традиционное тесто", "тонкое тесто"],
    additionalIngredients: [
      {
        id: 1,
        title: "Сырный бортик",
        price: {
          small: 169,
          medium: 169,
          big: 189,
        },
        img: cheeseBorder,
        isActive: true,
      },
      {
        id: 2,
        title: "Острый халапеньо",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: halapenyo,
        isActive: true,
      },
      {
        id: 9,
        title: "Цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: chicken,
        isActive: true,
      },
      {
        id: 4,
        title: "Ветчина",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: ham,
        isActive: true,
      },
      {
        id: 5,
        title: "Острый цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: spicyChicken,
        isActive: true,
      },
      {
        id: 7,
        title: "Острый чоризо",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: chorizo,
        isActive: true,
      },
    ],
    products: {
      traditional: [
        {
          title: "small",
          ccal: 228.9,
          protein: 11.2,
          fat: 8.6,
          carbs: 25.1,
          weight: 480,
          size: 25,
        },
        {
          title: "medium",
          ccal: 244.3,
          protein: 10.6,
          fat: 9.4,
          carbs: 27.6,
          weight: 640,
          size: 30,
        },
        {
          title: "big",
          ccal: 244.6,
          protein: 10.9,
          fat: 9.5,
          carbs: 27.1,
          weight: 870,
          size: 35,
        },
      ],
      thin: [
        {
          title: "medium",
          ccal: 242,
          protein: 11.5,
          fat: 10.6,
          carbs: 23.4,
          weight: 530,
          size: 30,
        },
        {
          title: "big",
          ccal: 251.3,
          protein: 12,
          fat: 10.8,
          carbs: 24.7,
          weight: 730,
          size: 35,
        },
      ],
    },
    ingredients: [
      {
        title: "Свежие листья салата айсберг",
        isIncluded: false,
      },
      {
        title: "цыпленок",
        isIncluded: true,
      },
      {
        title: "томаты черри",
        isIncluded: true,
      },
      {
        title: "сыры чеддер и пармезан",
        isIncluded: false,
      },
      {
        title: "моцарелла",
        isIncluded: false,
      },
      {
        title: "соус альфредо",
        isIncluded: false,
      },
      {
        title: "соус цезарь",
        isIncluded: false,
      },
    ],
  },
  {
    id: 4,
    title: "Цыпленок блю чиз",
    description: {
      traditional: [
        {
          title: "small",
          sm: 25,
          dough: "традиционное тесто",
          weight: 410,
        },
        {
          title: "medium",
          sm: 30,
          dough: "традиционное тесто",
          weight: 610,
        },
        {
          title: "big",
          sm: 35,
          dough: "традиционное тесто",
          weight: 830,
        },
      ],
      thin: [
        {
          title: "medium",
          sm: 30,
          dough: "тонкое тесто",
          weight: 500,
        },
        {
          title: "big",
          sm: 35,
          dough: "тонкое тесто",
          weight: 700,
        },
      ],
    },
    pizzaSize: [
      {
        title: "Маленькая",
        size: 25,
        price: 445,
        weight: {
          thin: "",
          traditional: 410,
        },
        img: {
          thin: "",
          traditional:
            "img/chickenBlueCheese/chickenBlueCheese-traditional-small.jpeg",
        },
      },
      {
        title: "Средняя",
        size: 30,
        price: 665,
        weight: {
          thin: 500,
          traditional: 610,
        },
        img: {
          thin: "img/chickenBlueCheese/chickenBlueCheese-thin-medium.jpeg",
          traditional:
            "img/chickenBlueCheese/chickenBlueCheese-traditional-medium.jpeg",
        },
      },
      {
        title: "Большая",
        size: 35,
        price: 785,
        weight: {
          thin: 700,
          traditional: 830,
        },
        img: {
          thin: "img/chickenBlueCheese/chickenBlueCheese-thin-big.jpeg",
          traditional:
            "img/chickenBlueCheese/chickenBlueCheese-traditional-big.jpeg",
        },
      },
    ],
    dough: ["традиционное тесто", "тонкое тесто"],
    additionalIngredients: [
      {
        id: 1,
        title: "Сырный бортик",
        price: {
          small: 169,
          medium: 169,
          big: 189,
        },
        img: cheeseBorder,
        isActive: true,
      },
      {
        id: 4,
        title: "Ветчина",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: ham,
        isActive: true,
      },
      {
        id: 2,
        title: "Острый халапеньо",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: halapenyo,
        isActive: true,
      },
      {
        id: 3,
        title: "Чеддер и пармезан",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: cheddarParmezan,
        isActive: true,
      },
      {
        id: 5,
        title: "Острый цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: spicyChicken,
        isActive: true,
      },
    ],
    products: {
      traditional: [
        {
          title: "small",
          ccal: 250.4,
          protein: 10.8,
          fat: 9.4,
          carbs: 29,
          weight: 410,
          size: 25,
        },
        {
          title: "medium",
          ccal: 245.7,
          protein: 10.9,
          fat: 9,
          carbs: 28.6,
          weight: 610,
          size: 30,
        },
        {
          title: "big",
          ccal: 242.3,
          protein: 10.9,
          fat: 8.5,
          carbs: 28.1,
          weight: 830,
          size: 35,
        },
      ],
      thin: [
        {
          title: "medium",
          ccal: 243.6,
          protein: 11.9,
          fat: 10.3,
          carbs: 23.4,
          weight: 500,
          size: 30,
        },
        {
          title: "big",
          ccal: 245.4,
          protein: 11.9,
          fat: 9.9,
          carbs: 25.4,
          weight: 700,
          size: 35,
        },
      ],
    },
    ingredients: [
      {
        title: "Цыпленок",
        isIncluded: true,
      },
      {
        title: "сыр блю чиз",
        isIncluded: true,
      },
      {
        title: "томаты",
        isIncluded: true,
      },
      {
        title: "моцарелла",
        isIncluded: false,
      },
      {
        title: "соус альфредо",
        isIncluded: false,
      },
    ],
  },
  {
    id: 5,
    title: "Нежный лосось",
    description: {
      traditional: [
        {
          title: "small",
          sm: 25,
          dough: "традиционное тесто",
          weight: 430,
        },
        {
          title: "medium",
          sm: 30,
          dough: "традиционное тесто",
          weight: 650,
        },
        {
          title: "big",
          sm: 35,
          dough: "традиционное тесто",
          weight: 880,
        },
      ],
      thin: [
        {
          title: "medium",
          sm: 30,
          dough: "тонкое тесто",
          weight: 540,
        },
        {
          title: "big",
          sm: 35,
          dough: "тонкое тесто",
          weight: 760,
        },
      ],
    },
    pizzaSize: [
      {
        title: "Маленькая",
        size: 25,
        price: 495,
        weight: {
          thin: "",
          traditional: 430,
        },
        img: {
          thin: "",
          traditional:
            "img/tender-salmon/tender-salomon-traditional-small.jpeg",
        },
      },
      {
        title: "Средняя",
        size: 30,
        price: 765,
        weight: {
          thin: 540,
          traditional: 650,
        },
        img: {
          thin: "img/tender-salmon/tender-salomon-thin-medium.jpeg",
          traditional:
            "img/tender-salmon/tender-salomon-traditional-medium.jpeg",
        },
      },
      {
        title: "Большая",
        size: 35,
        price: 895,
        weight: {
          thin: 760,
          traditional: 880,
        },
        img: {
          thin: "img/tender-salmon/tender-salomon-thin-large.jpeg",
          traditional:
            "img/tender-salmon/tender-salomon-traditional-large.jpeg",
        },
      },
    ],
    dough: ["традиционное тесто", "тонкое тесто"],
    additionalIngredients: [
      {
        id: 1,
        title: "Сырный бортик",
        price: {
          small: 169,
          medium: 169,
          big: 189,
        },
        img: cheeseBorder,
        isActive: true,
      },
      {
        id: 3,
        title: "Чеддер и пармезан",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: cheddarParmezan,
        isActive: true,
      },
      {
        id: 16,
        title: "Маслины",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: olives,
        isActive: true,
      },
      {
        id: 13,
        title: "Красный лук",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: redOnion,
        isActive: true,
      },
      {
        id: 5,
        title: "Острый цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: spicyChicken,
        isActive: true,
      },
      {
        id: 10,
        title: "Брынза",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: brynza,
        isActive: true,
      },
    ],
    products: {
      traditional: [
        {
          title: "small",
          ccal: 253.9,
          protein: 10.5,
          fat: 10.5,
          carbs: 27.7,
          weight: 430,
          size: 25,
        },
        {
          title: "medium",
          ccal: 243.4,
          protein: 10.7,
          fat: 9.6,
          carbs: 26.9,
          weight: 650,
          size: 30,
        },
        {
          title: "big",
          ccal: 240.4,
          protein: 10.8,
          fat: 9.4,
          carbs: 26.5,
          weight: 880,
          size: 35,
        },
      ],
      thin: [
        {
          title: "medium",
          ccal: 241,
          protein: 11.6,
          fat: 10.9,
          carbs: 22.5,
          weight: 540,
          size: 30,
        },
        {
          title: "big",
          ccal: 239.7,
          protein: 11.6,
          fat: 10.4,
          carbs: 23.4,
          weight: 760,
          size: 35,
        },
      ],
    },
    ingredients: [
      {
        title: "Лосось",
        isIncluded: true,
      },
      {
        title: "томаты черри",
        isIncluded: true,
      },
      {
        title: "соус песто",
        isIncluded: false,
      },
      {
        title: "моцарелла",
        isIncluded: false,
      },
      {
        title: "соус альфредо",
        isIncluded: false,
      },
    ],
  },
  {
    id: 6,
    title: "Додо микс",
    description: {
      traditional: [
        {
          title: "small",
          sm: 25,
          dough: "традиционное тесто",
          weight: 440,
        },
        {
          title: "medium",
          sm: 30,
          dough: "традиционное тесто",
          weight: 670,
        },
        {
          title: "big",
          sm: 35,
          dough: "традиционное тесто",
          weight: 980,
        },
      ],
      thin: [
        {
          title: "medium",
          sm: 30,
          dough: "тонкое тесто",
          weight: 570,
        },
        {
          title: "big",
          sm: 35,
          dough: "тонкое тесто",
          weight: 880,
        },
      ],
    },
    pizzaSize: [
      {
        title: "Маленькая",
        size: 25,
        price: 445,
        weight: {
          thin: "",
          traditional: 440,
        },
        img: {
          thin: "img/dodo-mix/dodo-mix-small.jpeg",
          traditional: "img/p_6_big.png",
        },
      },
      {
        title: "Средняя",
        size: 30,
        price: 665,
        weight: {
          thin: 570,
          traditional: 670,
        },
        img: {
          thin: "img/dodo-mix/dodo-mix-middle-thin.jpeg",
          traditional: "img/dodo-mix/dodo-mix-middle-traditional.jpeg",
        },
      },
      {
        title: "Большая",
        size: 35,
        price: 785,
        weight: {
          thin: 880,
          traditional: 980,
        },
        img: {
          thin: "img/dodo-mix/dodo-mix-big-thin.jpeg",
          traditional: "img/dodo-mix/dodo-mix-big-traditional.jpeg",
        },
      },
    ],
    dough: ["традиционное тесто", "тонкое тесто"],
    additionalIngredients: [],
    products: {
      traditional: [
        {
          title: "small",
          ccal: 270.4,
          protein: 10.3,
          fat: 12.5,
          carbs: 27.3,
          weight: 440,
          size: 25,
        },
        {
          title: "medium",
          ccal: 262,
          protein: 10.4,
          fat: 11.9,
          carbs: 26.5,
          weight: 670,
          size: 30,
        },
        {
          title: "big",
          ccal: 255.7,
          protein: 10.7,
          fat: 12,
          carbs: 24.4,
          weight: 980,
          size: 35,
        },
      ],
      thin: [
        {
          title: "medium",
          ccal: 258.7,
          protein: 11,
          fat: 13.3,
          carbs: 21.9,
          weight: 570,
          size: 30,
        },
        {
          title: "big",
          ccal: 251.4,
          protein: 11.1,
          fat: 12.9,
          carbs: 20.9,
          weight: 880,
          size: 35,
        },
      ],
    },
    ingredients: [
      {
        title: "Бекон",
        isIncluded: true,
      },
      {
        title: "цыпленок",
        isIncluded: true,
      },
      {
        title: "ветчина",
        isIncluded: true,
      },
      {
        title: "сыр блю чиз",
        isIncluded: true,
      },
      {
        title: "сыры чеддер и пармезан",
        isIncluded: true,
      },
      {
        title: "соус песто",
        isIncluded: false,
      },
      {
        title: "кубики брынзы",
        isIncluded: true,
      },
      {
        title: "томаты черри",
        isIncluded: true,
      },
      {
        title: "красный лук",
        isIncluded: true,
      },
      {
        title: "моцарелла",
        isIncluded: false,
      },
      {
        title: "соус альфредо",
        isIncluded: false,
      },
      {
        title: "чеснок",
        isIncluded: true,
      },
      {
        title: "итальянские травы",
        isIncluded: true,
      },
    ],
  },
  {
    id: 7,
    title: "Пепперони",
    description: {
      traditional: [
        {
          title: "small",
          sm: 25,
          dough: "традиционное тесто",
          weight: 390,
        },
        {
          title: "medium",
          sm: 30,
          dough: "традиционное тесто",
          weight: 570,
        },
        {
          title: "big",
          sm: 35,
          dough: "традиционное тесто",
          weight: 740,
        },
      ],
      thin: [
        {
          title: "medium",
          sm: 30,
          dough: "тонкое тесто",
          weight: 460,
        },
        {
          title: "big",
          sm: 35,
          dough: "тонкое тесто",
          weight: 620,
        },
      ],
    },
    pizzaSize: [
      {
        title: "Маленькая",
        size: 25,
        price: 395,
        weight: {
          thin: "",
          traditional: 390,
        },
        img: {
          thin: "",
          traditional: "img/pepperoni/pepperoni-traditional-small.jpeg",
        },
      },
      {
        title: "Средняя",
        size: 30,
        price: 595,
        weight: {
          thin: 460,
          traditional: 570,
        },
        img: {
          thin: "img/pepperoni/pepperoni-thin-middle.jpeg",
          traditional: "img/pepperoni/pepperoni-traditional-middle.jpeg",
        },
      },
      {
        title: "Большая",
        size: 35,
        price: 745,
        weight: {
          thin: 620,
          traditional: 740,
        },
        img: {
          thin: "img/pepperoni/pepperoni-thin-big.jpeg",
          traditional: "img/pepperoni/pepperoni-traditional-big.jpeg",
        },
      },
    ],
    dough: ["традиционное тесто", "тонкое тесто"],
    additionalIngredients: [
      {
        id: 1,
        title: "Сырный бортик",
        price: {
          small: 169,
          medium: 169,
          big: 189,
        },
        img: cheeseBorder,
        isActive: true,
      },
      {
        id: 2,
        title: "Острый халапеньо",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: halapenyo,
        isActive: true,
      },
      {
        id: 12,
        title: "Сыр блю чиз",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: blueCheese,
        isActive: true,
      },
      {
        id: 11,
        title: "Шампиньоны",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: mushrooms,
        isActive: true,
      },
      {
        id: 5,
        title: "Острый цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: spicyChicken,
        isActive: true,
      },
      {
        id: 3,
        title: "Чеддер и пармезан",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: cheddarParmezan,
        isActive: true,
      },
    ],
    products: {
      traditional: [
        {
          title: "small",
          ccal: 271.9,
          protein: 10.8,
          fat: 10.9,
          carbs: 30.7,
          weight: 390,
          size: 25,
        },
        {
          title: "medium",
          ccal: 273,
          protein: 11,
          fat: 11,
          carbs: 30.7,
          weight: 570,
          size: 30,
        },
        {
          title: "big",
          ccal: 272.3,
          protein: 10.8,
          fat: 10.7,
          carbs: 31.4,
          weight: 740,
          size: 35,
        },
      ],
      thin: [
        {
          title: "medium",
          ccal: 277.3,
          protein: 12.2,
          fat: 12.8,
          carbs: 26.5,
          weight: 460,
          size: 30,
        },
        {
          title: "big",
          ccal: 277.7,
          protein: 11.7,
          fat: 12.1,
          carbs: 28.6,
          weight: 620,
          size: 35,
        },
      ],
    },
    ingredients: [
      {
        title: "Пикантная пепперони",
        isIncluded: true,
      },
      {
        title: "увеличенная порция моцареллы",
        isIncluded: false,
      },
      {
        title: "томатный соус",
        isIncluded: false,
      },
    ],
  },
  {
    id: 8,
    title: "Карбонара",
    description: {
      traditional: [
        {
          title: "small",
          sm: 25,
          dough: "традиционное тесто",
          weight: 420,
        },
        {
          title: "medium",
          sm: 30,
          dough: "традиционное тесто",
          weight: 620,
        },
        {
          title: "big",
          sm: 35,
          dough: "традиционное тесто",
          weight: 840,
        },
      ],
      thin: [
        {
          title: "medium",
          sm: 30,
          dough: "тонкое тесто",
          weight: 520,
        },
        {
          title: "big",
          sm: 35,
          dough: "тонкое тесто",
          weight: 700,
        },
      ],
    },
    pizzaSize: [
      {
        title: "Маленькая",
        size: 25,
        price: 395,
        weight: {
          thin: "",
          traditional: 410,
        },
        img: {
          thin: "",
          traditional: "img/carbonara/carbonara-traditional-small.jpeg",
        },
      },
      {
        title: "Средняя",
        size: 30,
        price: 595,
        weight: {
          thin: 500,
          traditional: 610,
        },
        img: {
          thin: "img/carbonara/carbonara-thin-medium.jpeg",
          traditional: "img/carbonara/carbonara-traditional-medium.jpeg",
        },
      },
      {
        title: "Большая",
        size: 35,
        price: 745,
        weight: {
          thin: 700,
          traditional: 820,
        },
        img: {
          thin: "img/carbonara/carbonara-thin-big.jpeg",
          traditional: "img/carbonara/carbonara-traditional-big.jpeg",
        },
      },
    ],
    dough: ["традиционное тесто", "тонкое тесто"],
    additionalIngredients: [
      {
        id: 1,
        title: "Сырный бортик",
        price: {
          small: 169,
          medium: 169,
          big: 189,
        },
        img: cheeseBorder,
        isActive: true,
      },
      {
        id: 2,
        title: "Острый халапеньо",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: halapenyo,
        isActive: true,
      },
      {
        id: 7,
        title: "Острая чоризо",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: chorizo,
        isActive: true,
      },
      {
        id: 9,
        title: "Цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: chicken,
        isActive: true,
      },
      {
        id: 5,
        title: "Острый цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: spicyChicken,
        isActive: true,
      },
      {
        id: 4,
        title: "Ветчина",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: ham,
        isActive: true,
      },
    ],
    products: {
      traditional: [
        {
          title: "small",
          ccal: 289.2,
          protein: 9.4,
          fat: 14.3,
          carbs: 28.6,
          weight: 420,
          size: 25,
        },
        {
          title: "medium",
          ccal: 285,
          protein: 9.4,
          fat: 13.9,
          carbs: 28.5,
          weight: 620,
          size: 30,
        },
        {
          title: "big",
          ccal: 286.8,
          protein: 9.4,
          fat: 14.3,
          carbs: 28.1,
          weight: 840,
          size: 35,
        },
      ],
      thin: [
        {
          title: "medium",
          ccal: 285.8,
          protein: 9.9,
          fat: 15.9,
          carbs: 23.8,
          weight: 520,
          size: 30,
        },
        {
          title: "big",
          ccal: 293.8,
          protein: 10,
          fat: 16.1,
          carbs: 25.1,
          weight: 720,
          size: 35,
        },
      ],
    },
    ingredients: [
      {
        title: "Бекон",
        isIncluded: true,
      },
      {
        title: "сыры чеддер и пармезан",
        isIncluded: true,
      },
      {
        title: "моцарелла",
        isIncluded: false,
      },
      {
        title: "томаты черри",
        isIncluded: true,
      },
      {
        title: "красный лук",
        isIncluded: true,
      },
      {
        title: "чеснок",
        isIncluded: true,
      },
      {
        title: "соус альфредо",
        isIncluded: false,
      },
      {
        title: "итальянские травы",
        isIncluded: true,
      },
    ],
  },
  {
    id: 9,
    title: "Сырная",
    description: {
      traditional: [
        {
          title: "small",
          sm: 25,
          dough: "традиционное тесто",
          weight: 360,
        },
        {
          title: "medium",
          sm: 30,
          dough: "традиционное тесто",
          weight: 540,
        },
        {
          title: "big",
          sm: 35,
          dough: "традиционное тесто",
          weight: 710,
        },
      ],
      thin: [
        {
          title: "medium",
          sm: 30,
          dough: "тонкое тесто",
          weight: 430,
        },
        {
          title: "big",
          sm: 35,
          dough: "тонкое тесто",
          weight: 600,
        },
      ],
    },
    pizzaSize: [
      {
        title: "Маленькая",
        size: 25,
        price: 245,
        weight: {
          thin: "",
          traditional: 360,
        },
        img: {
          thin: "",
          traditional: "img/cheese/cheese-traditional-small.jpeg",
        },
      },
      {
        title: "Средняя",
        size: 30,
        price: 435,
        weight: {
          thin: 430,
          traditional: 540,
        },
        img: {
          thin: "img/cheese/cheese-thin-medium.jpeg",
          traditional: "img/cheese/cheese-traditional-medium.jpeg",
        },
      },
      {
        title: "Большая",
        size: 35,
        price: 575,
        weight: {
          thin: 600,
          traditional: 710,
        },
        img: {
          thin: "img/cheese/cheese-thin-big.jpeg",
          traditional: "img/cheese/cheese-traditional-big.jpeg",
        },
      },
    ],
    dough: ["традиционное тесто", "тонкое тесто"],
    additionalIngredients: [
      {
        id: 1,
        title: "Сырный бортик",
        price: {
          small: 169,
          medium: 169,
          big: 189,
        },
        img: cheeseBorder,
        isActive: false,
      },
      {
        id: 4,
        title: "Ветчина",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: ham,
        isActive: true,
      },
      {
        id: 9,
        title: "Цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: chicken,
        isActive: true,
      },
      {
        id: 10,
        title: "Брынза",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: brynza,
        isActive: true,
      },
      {
        id: 5,
        title: "Острый цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: spicyChicken,
        isActive: true,
      },
      {
        id: 6,
        title: "Томаты",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: tomatoes,
        isActive: true,
      },
    ],
    products: {
      traditional: [
        {
          title: "small",
          ccal: 282.5,
          protein: 11.2,
          fat: 11,
          carbs: 32.7,
          weight: 360,
          size: 25,
        },
        {
          title: "medium",
          ccal: 280.4,
          protein: 11.4,
          fat: 11.1,
          carbs: 31.9,
          weight: 540,
          size: 30,
        },
        {
          title: "big",
          ccal: 285.4,
          protein: 11.5,
          fat: 11.3,
          carbs: 32.4,
          weight: 710,
          size: 35,
        },
      ],
      thin: [
        {
          title: "medium",
          ccal: 286.9,
          protein: 12.7,
          fat: 13,
          carbs: 27.8,
          weight: 430,
          size: 30,
        },
        {
          title: "big",
          ccal: 288.8,
          protein: 12.4,
          fat: 12.7,
          carbs: 29.2,
          weight: 600,
          size: 35,
        },
      ],
    },
    ingredients: [
      {
        title: "Увеличенная порция моцареллы",
        isIncluded: false,
      },
      {
        title: "сыры чеддер и пармезан",
        isIncluded: false,
      },
      {
        title: "соус альфредо",
        isIncluded: false,
      },
    ],
  },
  {
    id: 10,
    title: "Мексиканская",
    description: {
      traditional: [
        {
          title: "small",
          sm: 25,
          dough: "традиционное тесто",
          weight: 480,
        },
        {
          title: "medium",
          sm: 30,
          dough: "традиционное тесто",
          weight: 710,
        },
        {
          title: "big",
          sm: 35,
          dough: "традиционное тесто",
          weight: 960,
        },
      ],
      thin: [
        {
          title: "medium",
          sm: 30,
          dough: "тонкое тесто",
          weight: 600,
        },
        {
          title: "big",
          sm: 35,
          dough: "тонкое тесто",
          weight: 840,
        },
      ],
    },
    pizzaSize: [
      {
        title: "Маленькая",
        size: 25,
        price: 445,
        weight: {
          thin: "",
          traditional: 480,
        },
        img: {
          thin: "",
          traditional: "img/mexican/mexican-traditional-small.jpeg",
        },
      },
      {
        title: "Средняя",
        size: 30,
        price: 665,
        weight: {
          thin: 600,
          traditional: 710,
        },
        img: {
          thin: "img/mexican/mexican-thin-medium.jpeg",
          traditional: "img/mexican/mexican-traditional-medium.jpeg",
        },
      },
      {
        title: "Большая",
        size: 35,
        price: 785,
        weight: {
          thin: 840,
          traditional: 960,
        },
        img: {
          thin: "img/mexican/mexican-thin-large.jpeg",
          traditional: "img/mexican/mexican-traditional-large.jpeg",
        },
      },
    ],
    dough: ["традиционное тесто", "тонкое тесто"],
    additionalIngredients: [
      {
        id: 1,
        title: "Сырный бортик",
        price: {
          small: 169,
          medium: 169,
          big: 189,
        },
        img: cheeseBorder,
        isActive: true,
      },
      {
        id: 8,
        title: "Моцарелла",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: mozarella,
        isActive: true,
      },
      {
        id: 2,
        title: "Острый халапеньо",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: halapenyo,
        isActive: true,
      },
      {
        id: 7,
        title: "Острая чоризо",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: chorizo,
        isActive: true,
      },
      {
        id: 5,
        title: "Острый цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: spicyChicken,
        isActive: true,
      },
      {
        id: 3,
        title: "Чеддер и пармезан",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: cheddarParmezan,
        isActive: true,
      },
    ],
    products: {
      traditional: [
        {
          title: "small",
          ccal: 191.6,
          protein: 8.5,
          fat: 4.9,
          carbs: 27.1,
          weight: 480,
          size: 25,
        },
        {
          title: "medium",
          ccal: 192.2,
          protein: 8.8,
          fat: 5,
          carbs: 26.7,
          weight: 710,
          size: 30,
        },
        {
          title: "big",
          ccal: 188.6,
          protein: 8.8,
          fat: 4.7,
          carbs: 26.4,
          weight: 960,
          size: 35,
        },
      ],
      thin: [
        {
          title: "medium",
          ccal: 180.6,
          protein: 9.2,
          fat: 5.3,
          carbs: 22.9,
          weight: 600,
          size: 30,
        },
        {
          title: "big",
          ccal: 180.6,
          protein: 9.2,
          fat: 4.9,
          carbs: 23.6,
          weight: 840,
          size: 35,
        },
      ],
    },
    ingredients: [
      {
        title: "Острый цыпленок",
        isIncluded: true,
      },
      {
        title: "острый перец халапеньо",
        isIncluded: true,
      },
      {
        title: "соус сальса",
        isIncluded: false,
      },
      {
        title: "томаты",
        isIncluded: true,
      },
      {
        title: "сладкий перец",
        isIncluded: true,
      },
      {
        title: "красный лук",
        isIncluded: true,
      },
      {
        title: "моцарелла",
        isIncluded: false,
      },
      {
        title: "томатный соус",
        isIncluded: false,
      },
    ],
  },
  {
    id: 11,
    title: "Супермясная",
    description: {
      traditional: [
        {
          title: "small",
          sm: 25,
          dough: "традиционное тесто",
          weight: 440,
        },
        {
          title: "medium",
          sm: 30,
          dough: "традиционное тесто",
          weight: 640,
        },
        {
          title: "big",
          sm: 35,
          dough: "традиционное тесто",
          weight: 870,
        },
      ],
      thin: [
        {
          title: "medium",
          sm: 30,
          dough: "тонкое тесто",
          weight: 530,
        },
        {
          title: "big",
          sm: 35,
          dough: "тонкое тесто",
          weight: 750,
        },
      ],
    },
    pizzaSize: [
      {
        title: "Маленькая",
        size: 25,
        price: 495,
        weight: {
          thin: "",
          traditional: 440,
        },
        img: {
          thin: "",
          traditional: "img/supermeat/supermeat-traditional-small.jpeg",
        },
      },
      {
        title: "Средняя",
        size: 30,
        price: 765,
        weight: {
          thin: 530,
          traditional: 640,
        },
        img: {
          thin: "img/supermeat/supermeat-thin-medium.jpeg",
          traditional: "img/supermeat/supermeat-traditional-medium.jpeg",
        },
      },
      {
        title: "Большая",
        size: 35,
        price: 895,
        weight: {
          thin: 750,
          traditional: 870,
        },
        img: {
          thin: "img/supermeat/supermeat-thin-big.jpeg",
          traditional: "img/supermeat/supermeat-traditional-big.jpeg",
        },
      },
    ],
    dough: ["традиционное тесто", "тонкое тесто"],
    additionalIngredients: [
      {
        id: 1,
        title: "Сырный бортик",
        price: {
          small: 169,
          medium: 169,
          big: 189,
        },
        img: cheeseBorder,
        isActive: true,
      },
      {
        id: 2,
        title: "Острый халапеньо",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: halapenyo,
        isActive: true,
      },
      {
        id: 3,
        title: "Чеддер и пармезан",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: cheddarParmezan,
        isActive: true,
      },
      {
        id: 4,
        title: "Ветчина",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: ham,
        isActive: true,
      },
      {
        id: 5,
        title: "Острый цыпленок",
        price: {
          small: 49,
          medium: 59,
          big: 79,
        },
        img: spicyChicken,
        isActive: true,
      },
      {
        id: 6,
        title: "Томаты",
        price: {
          small: 29,
          medium: 39,
          big: 49,
        },
        img: tomatoes,
        isActive: true,
      },
    ],
    products: {
      traditional: [
        {
          title: "small",
          ccal: 295.4,
          protein: 10.6,
          fat: 15.1,
          carbs: 27.2,
          weight: 440,
          size: 25,
        },
        {
          title: "medium",
          ccal: 297.1,
          protein: 11,
          fat: 15.1,
          carbs: 27.3,
          weight: 640,
          size: 30,
        },
        {
          title: "big",
          ccal: 298,
          protein: 11,
          fat: 15.4,
          carbs: 26.7,
          weight: 870,
          size: 35,
        },
      ],
      thin: [
        {
          title: "medium",
          ccal: 305.8,
          protein: 11.9,
          fat: 17.5,
          carbs: 23,
          weight: 530,
          size: 30,
        },
        {
          title: "big",
          ccal: 306.6,
          protein: 11.8,
          fat: 17.4,
          carbs: 23.6,
          weight: 750,
          size: 35,
        },
      ],
    },
    ingredients: [
      {
        title: "Пикатная пепперони",
        isIncluded: true,
      },
      {
        title: "цыпленок",
        isIncluded: true,
      },
      {
        title: "острая чоризо",
        isIncluded: true,
      },
      {
        title: "бекон",
        isIncluded: true,
      },
      {
        title: "митболы из говядины",
        isIncluded: true,
      },
      {
        title: "моцарелла",
        isIncluded: false,
      },
      {
        title: "томатный соус",
        isIncluded: false,
      },
    ],
  },
];
