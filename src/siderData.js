import cezar from "./img/newPopSliderPhotos/cezar.jpeg";
import carbonara from "./img/newPopSliderPhotos/carbonara.jpeg";
import pizzas2 from "./img/newPopSliderPhotos/2pizzas.png";
import salmon from "./img/newPopSliderPhotos/salmon.jpeg";
import cheddar from "./img/newPopSliderPhotos/cheddar.png";
import beverage from "./img/newPopSliderPhotos/beverage.jpeg";
import cheesecake from "./img/newPopSliderPhotos/cheesecake.png";
import dododmix from "./img/newPopSliderPhotos/dodoMix.jpeg";
import soup from "./img/newPopSliderPhotos/soup.jpeg";
import chickenCheese from "./img/newPopSliderPhotos/chickenCheese.jpeg";

const slideData = [
  {
    index: 0,
    headline: "Цезарь",
    price: "от 445 ₽",
    src: cezar,
  },
  {
    index: 1,
    headline: "Карбонара",
    price: "от 395 ₽",
    src: carbonara,
  },
  {
    index: 2,
    headline: "2 пиицы",
    price: "899 ₽",
    src: pizzas2,
  },
  {
    index: 3,
    headline: "Нежный лосось",
    price: "от 495 ₽",
    src: salmon,
  },
  {
    index: 4,
    headline: "Чиззи чеддер",
    price: "от 395 ₽",
    src: cheddar,
  },
  {
    index: 5,
    headline: "Цыпленок блю чиз",
    price: "от 445 ₽",
    src: chickenCheese,
  },
  {
    index: 6,
    headline: "Додо микс",
    price: "от 445 ₽",
    src: dododmix,
  },
  {
    index: 7,
    headline: "Чизкейк Нью-Йорк",
    price: "от 129 ₽",
    src: cheesecake,
  },
  {
    index: 8,
    headline: "Coca-Cola Orange",
    price: "95 ₽",
    src: beverage,
  },
];
export default slideData;
