import slide1 from "./img/banner_slider/slid_1.jpeg";
import slide2 from "./img/banner_slider/slid_2.jpeg";
import slide3 from "./img/banner_slider/slid_3.jpeg";
import slide4 from "./img/banner_slider/slid_4.jpeg";
import slide5 from "./img/banner_slider/slid_5.jpeg";

export const banners = [
  {
    image: slide1,
    index: 0,
  },
  {
    image: slide2,
    index: 1,
  },
  {
    image: slide3,
    index: 2,
  },
  {
    image: slide4,
    index: 3,
  },
  {
    image: slide5,
    index: 4,
  },
];
