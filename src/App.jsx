import "./App.css";
import React from "react";
import Header from "./Components/header/mobHeader/Header";
import Main from "./Components/main/Main";
import Footer from "./Components/footer/Footer";

function App() {
  return (
    <>
      <div className="wrapper">
        <Header />
        <Main />
      </div>
      <Footer />
    </>
  );
}

export default App;
