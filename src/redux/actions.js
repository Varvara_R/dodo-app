import { ADD_TO_BASKET, DELETE_FROM_CART } from "./types";

export function addToBasket(product) {
  return {
    type: ADD_TO_BASKET,
    payload: product,
  };
}
export function deleteFromBasket(product) {
  return {
    type: DELETE_FROM_CART,
    payload: product,
  };
}
