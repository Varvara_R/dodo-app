import { ADD_TO_BASKET } from "./types";

const initialState = {
  products: [],
};
export const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_BASKET:
      if (
        state.products.some(
          (elem) => elem.createIdentifier === action.payload.createIdentifier
        )
      ) {
        state.products.map((elem) => {
          if (elem.createIdentifier === action.payload.createIdentifier) {
            elem.quantity++;
            return { ...state };
          }
        });
      } else {
        return { ...state, products: [...state.products, action.payload] };
      }
    default:
      return state;
  }
};
