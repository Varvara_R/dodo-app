export let beverages = [
  {
    id: 32,
    title: "2 Coca-Cola по суперцене",
    img: "img/bev_1.jpeg",
    description: "Две классические Coca-Cola по 0,5 литров по выгодной цене",
    oldPrice: 199,
    price: 165,
    createIdentifier: 32,
    quantity: 1,
    volume: "0,5 л",
  },
  {
    id: 33,
    title: "Coca-Cola Orange",
    img: "img/bev_2.jpeg",
    description: null,
    oldPrice: 109,
    price: 95,
    createIdentifier: 33,
    quantity: 1,
    volume: "0,5 л",
  },
  {
    id: 34,
    title: "Sprite",
    img: "img/bev_3.jpeg",
    description: null,
    oldPrice: 109,
    price: 95,
    createIdentifier: 34,
    quantity: 1,
    volume: "0,5 л",
  },
  {
    id: 35,
    title: "Schweppes bitter lemon",
    img: "img/bev_4.jpeg",
    description: null,
    oldPrice: 109,
    price: 75,
    createIdentifier: 35,
    quantity: 1,
    volume: "0,33 л",
  },
  {
    id: 36,
    title: "FuzeTea Черный с лимоном и лемонграссом",
    img: "img/bev_5.jpeg",
    description: null,
    oldPrice: 109,
    price: 95,
    createIdentifier: 36,
    quantity: 1,
    volume: "0,5 л",
  },
  {
    id: 37,
    title: "FuzeTea Зеленый с манго и ромашкой",
    img: "img/bev_6.jpeg",
    description: null,
    oldPrice: 109,
    price: 95,
    createIdentifier: 37,
    quantity: 1,
    volume: "0,5 л",
  },
  {
    id: 38,
    title: "Какао с маршмеллоу",
    img: "img/bev_7.jpeg",
    description:
      "Горячий какао с молоком, молочной пенкой и ванильным маршмеллоу",
    oldPrice: 129,
    price: 115,
    energy: {
      ccal: 68.8,
      protein: 1.9,
      fats: 1.7,
      carbos: 11,
      weight: 270,
    },
    createIdentifier: 38,
    quantity: 1,
    volume: "0,3 л",
  },
  {
    id: 39,
    title: "Кофе Карамельный капучино",
    img: "img/bev_8.jpeg",
    description:
      "Горячий напиток на основе эспрессо со вспененным молоком и сиропом со вкусом карамели",
    oldPrice: 149,
    price: 135,
    energy: {
      ccal: 69.8,
      protein: 2.5,
      fats: 2.8,
      carbos: 8.2,
      weight: 230,
    },
    createIdentifier: 39,
    quantity: 1,
    volume: "0,3 л",
  },
];
